## Hiboutik

Hiboutik connector for Dokos and ERPNext.
This connector has been developed to synchronize the sales documents created in Hiboutik with Dokos or ERPNext.

### Functionalities

- Synchronisation of item details from Dokos/ERPNext to Hiboutik
- Synchronisation of individual customers from Dokos/ERPNext to Hiboutik
- Creation of Dokos/ERPNext sales invoices for each Hiboutik sale
- Stock balance synchronisation for each item


### Current limitations

- It is only possible to use a single default warehouse
- Customer loyalty points are not handled in Dokos

### Documentation

https://doc.dokos.io/fr/integrations/hiboutik


### Installation

This connector is compatible with Dokos.

Branches:
- Version 2: `master`
- Version 3: `v3.x.x`

To use it, you need to install Dokoson a server first.
Then you can get this application in you bench using the following command:

`bench get-app https://gitlab.com/dokos/hiboutik.git --branch <branchname>`

You can then choose to install it on your site.
For example, if you site is named demo.com:

`bench --site demo.com install-app hiboutik`


#### Credits

The first version of this connector has been written by ioCraft.
Source code: https://github.com/io-craft-org/opossum

The developments are funded by the following third places:

- https://www.cooperativebaraka.fr/
- https://lacoroutine.org/
- MonsFabrica

#### License

GPLv3
