import frappe
from frappe.utils import cint

def after_bin_update(doc, method):
	if (
		not frappe.conf.developer_mode and
		doc.flags.via_stock_ledger_entry and
		cint(frappe.db.get_single_value("Hiboutik Settings", "enable_sync")) and
		cint(frappe.db.get_single_value("Hiboutik Settings", "stock_synchronization"))
	):
		frappe.enqueue("hiboutik.controllers.item.sync_item", item=doc.item_code)
