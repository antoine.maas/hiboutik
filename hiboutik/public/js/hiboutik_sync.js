frappe.ui.form.on('Item', 'refresh', (frm) => {
	if (frm.doc.sync_with_hiboutik) {
		frappe.db.get_value("Hiboutik Settings", "Hiboutik Settings", "enable_sync", r => {
			if (r.enable_sync == "1") {
				frm.add_custom_button(__('Synchronize with Hiboutik'), () => {
					frappe.call({
						type:"POST",
						method:"hiboutik.controllers.item.sync_item",
						args: {
							item: frm.doc.name
						},
					}).done(() => {
						frappe.show_alert({
								indicator: "green",
								message: __("Item synced to Hiboutik")
						});
					}).fail(() => {
						frappe.show_alert({
							indicator: "red",
							message: __("Item failed to synchronize to Hiboutik")
						});
					});
				});
			}
		})
	}
});
