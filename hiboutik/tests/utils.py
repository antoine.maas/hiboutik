import frappe
from erpnext.setup.utils import before_tests as before_erpnext_tests

def before_tests():
	before_erpnext_tests()

	create_french_company()
	frappe.db.set_value("Company", "_French Test Company", "cost_center", "Main - _FTC")

	settings = frappe.get_single("Hiboutik Settings")
	settings.run_method("create_custom_fields")
	settings.run_method("set_custom_properties")

	create_sales_items()
	setup_modes_of_payment()
	frappe.db.set_value("Stock Settings", None, "default_warehouse", "Stores - _FTC")
	frappe.db.set_value("Stock Settings", None, "allow_negative_stock", 1)

	pos_profile = create_pos_profile()

	settings.update(
		{
			"company": "_French Test Company",
			"pos_profile": pos_profile.name,
			"discount_account": "709 - Rabais, remises et ristournes accordés par l'entreprise - _FTC",
			"hiboutik_taxes": [
				{
					"hiboutik_rate": 20,
					"hiboutik_id": 1,
					"item_tax_template": "TVA 20% Collectée - _FTC"
				},
				{
					"hiboutik_rate": 10,
					"hiboutik_id": 2,
					"item_tax_template": "TVA 10% Collectée - _FTC"
				},
				{
					"hiboutik_rate": 5.5,
					"hiboutik_id": 3,
					"item_tax_template": "TVA 5.5% Collectée - _FTC"
				},
				{
					"hiboutik_rate": 2.1,
					"hiboutik_id": 4,
					"item_tax_template": "TVA 5.5% Collectée - _FTC"
				},
				{
					"hiboutik_rate": 0,
					"hiboutik_id": 5,
					"item_tax_template": "TVA 20% Collectée - _FTC"
				}
			],
			"hiboutik_payment_methods": [
				{
					"hiboutik_payment_method": "CB",
					"mode_of_payment": "Credit Card"
				},
				{
					"hiboutik_payment_method": "ESP",
					"mode_of_payment": "Cash"
				},
				{
					"hiboutik_payment_method": "CHE",
					"mode_of_payment": "Check"
				}
			]
		}
	)

	settings.flags.ignore_mandatory = True
	settings.save()

def create_french_company():
	return frappe.get_doc(
		{
			"abbr": "_FTC",
			"company_name": "_French Test Company",
			"country": "France",
			"default_currency": "EUR",
			"doctype": "Company",
			"domain": "Manufacturing",
			"chart_of_accounts": "Plan Comptable Général",
			"enable_perpetual_inventory": 0
		},
	).insert(ignore_if_duplicate=True)

def create_sales_items():
	for item in [
		{
			"doctype": "Item",
			"hiboutik_id": 5,
			"item_code": "_hiboutik_sales_item_1",
			"item_group": "Services",
			"stock_uom": "Unit",
			"opening_stock": 20,
			"valuation_rate": 250
		},
		{
			"doctype": "Item",
			"hiboutik_id": 6,
			"item_code": "_hiboutik_sales_item_2",
			"item_group": "Services",
			"stock_uom": "Unit",
			"opening_stock": 20,
			"valuation_rate": 120
		}
	]:

		frappe.get_doc(
			item
		).insert(ignore_if_duplicate=True)

def create_pos_profile():
	customer = create_customer()

	return frappe.get_doc(
		{
			"doctype": "POS Profile",
			"name": "_Hiboutik POS Profile",
			"customer": customer.name,
			"payments": [
				{
					"default": 1,
					"allow_in_returns": 0,
					"idx": 1,
					"mode_of_payment": "Cash",
				}
			],
			"company": "_French Test Company",
			"currency": "EUR",
			"write_off_cost_center": "Main - _FTC",
			"write_off_account": "658 - Charges diverses de gestion courante - _FTC",
			"warehouse": "Stores - _FTC",
		}
	).insert(ignore_if_duplicate=True)

def create_customer():
	return frappe.get_doc(
		{
			"doctype": "Customer",
			"customer_name": "POS Customer",
			"customer_group": "All Customer Groups"
		}
	).insert(ignore_if_duplicate=True)

def setup_modes_of_payment():
	for mop in frappe.get_all("Mode of Payment"):
		doc = frappe.get_doc("Mode of Payment", mop.name)
		doc.append("accounts", {
			"company": "_French Test Company",
			"default_account": "5121 - Comptes en monnaie nationale - _FTC"
		})
		try:
			doc.save()
		except Exception:
			continue