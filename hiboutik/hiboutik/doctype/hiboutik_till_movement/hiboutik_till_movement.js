// Copyright (c) 2022, Dokos SAS and contributors
// For license information, please see license.txt

frappe.ui.form.on('Hiboutik Till Movement', {
	setup(frm) {
		frm.make_methods = {
			'Journal Entry': () => {
				frm.events.make_new_journal_entry(frm);
			}
		}
	},

	make_new_journal_entry(frm) {
		return frappe.call({
			method: "hiboutik.hiboutik.doctype.hiboutik_till_movement.hiboutik_till_movement.make_journal_entry",
			args: {
				"name": frm.doc.name
			}
		}).then(r => {
			const doclist = frappe.model.sync(r.message);
			frappe.set_route("Form", doclist[0].doctype, doclist[0].name);
		});
	}
});
