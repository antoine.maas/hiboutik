# -*- coding: utf-8 -*-
# Copyright (c) 2021, ioCraft, Dokos and contributors
# For license information, please see license.txt
import frappe
from frappe import _
from frappe.custom.doctype.custom_field.custom_field import create_custom_field
from frappe.model.document import Document
from frappe.utils import flt

from frappe.custom.doctype.property_setter.property_setter import make_property_setter

from hiboutik.hiboutik.api import Webhook, HiboutikAPI, HiboutikAPIError, HiboutikConnector
from urllib.parse import urlparse


class HiboutikSettings(Document):
	def validate(self):
		if self.enable_sync:
			self.create_custom_fields()
			self.set_custom_properties()
			self.get_taxes_id()

	def on_update(self):
		if self.enable_sync:
			self.make_and_set_webhook_urls()

	def create_custom_fields(self):
		"""When the user enables sync, make custom fields in Doctypes"""
		for doctype in ["Item", "Sales Invoice", "Customer", "Address", "Journal Entry"]:
			fields = [
				dict(
					fieldname="hiboutik_id",
					label="Hiboutik ID",
					fieldtype="Data",
					read_only=1,
					print_hide=1,
					translatable=0,
					no_copy=1
				)
			]
			if doctype == "Item":
				fields.append(dict(
					fieldname="sync_with_hiboutik",
					label="Sync with Hiboutik",
					fieldtype="Check",
					insert_after="is_stock_item",
					print_hide=1,
				))
			if doctype == "Sales Invoice":
				fields.append(dict(
					fieldname="hiboutik_sales",
					label="Hiboutik Sales",
					fieldtype="Link",
					insert_after="external_reference",
					print_hide=1,
					options="Hiboutik Sales",
				))

			if doctype == "Journal Entry":
				fields = [
					dict(
						fieldname="hiboutik_till_movement",
						label="Hiboutik Till Movement",
						fieldtype="Link",
						insert_after="remark",
						print_hide=1,
						options="Hiboutik Till Movement",
					)
				]

			for df in fields:
				create_custom_field(doctype, df)

	def set_custom_properties(self):
		make_property_setter("POS Profile", "customer", "reqd", 1, "Check")

	def _make_pos_invoice_webhook_url(self):
		endpoint = "/api/method/hiboutik.webhooks.pos_invoice_webhook"
		return self._make_pos_webhook_url(endpoint)

	def _make_pos_customer_webhook_url(self):
		endpoint = "/api/method/hiboutik.webhooks.pos_customer_webhook"
		return self._make_pos_webhook_url(endpoint)

	def _make_pos_webhook_url(self, endpoint):
		"""Generate the public POS Invoice Webhook URL"""
		try:
			url = frappe.utils.get_url()
		except RuntimeError:
			# for CI Test to work
			url = "http://localhost:8000"

		server_url = "{uri.scheme}://{uri.netloc}".format(uri=urlparse(url))

		delivery_url = server_url + endpoint

		return delivery_url

	def make_and_set_webhook_urls(self):
		"""Make the Webhook URI and register it to Hiboutik"""
		self.pos_invoice_webhook = self._make_pos_invoice_webhook_url()
		self.pos_customer_webhook = self._make_pos_customer_webhook_url()

		hiboutik_api = HiboutikAPI(self)

		connector = HiboutikConnector(hiboutik_api)
		connector.delete_webhooks()

		for data in [
			{"field": "pos_invoice_webhook", "url": self.pos_invoice_webhook, "action": "sale", "label": _("Sales synchronisation with Dokos")},
			{"field": "pos_customer_webhook", "url": self.pos_customer_webhook, "action": "customer", "label": _("Customers synchronisation with Dokos")},
		]:
			webhook = Webhook.create_connector_webhook(
				data.get("label"),
				data.get("url"),
				data.get("action")
			)

			try:
				connector.set_webhook(webhook)
				self.db_set(data.get("field"), data.get("url"), commit=True)
			except HiboutikAPIError as e:
				frappe.msgprint(
					msg=str(e),
					title=_("Hiboutik API Error"),
					raise_exception=HiboutikAPIError,
				)

	def get_taxes_id(self):
		hiboutik_api = HiboutikAPI(self)

		try:
			taxes = hiboutik_api.get_taxes()
			for hiboutik_tax in self.hiboutik_taxes:
				corresponding_tax = [tax.tax_id for tax in taxes if flt(tax.tax_value) == flt(hiboutik_tax.hiboutik_rate) / 100 ]
				if corresponding_tax:
					hiboutik_tax.hiboutik_id = corresponding_tax[0]

			hiboutik_tax_ids = [tax.hiboutik_id for tax in self.hiboutik_taxes]
			for tax in taxes:
				if tax.tax_id not in hiboutik_tax_ids:
					self.append("hiboutik_taxes", {
						"hiboutik_rate": flt(tax.tax_value) * 100,
						"hiboutik_id": tax.tax_id
					})
		except HiboutikAPIError as e:
			frappe.msgprint(
				msg=str(e),
				title=_("Hiboutik API Error"),
				raise_exception=HiboutikAPIError,
			)
